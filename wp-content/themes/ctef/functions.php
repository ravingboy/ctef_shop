<?php
/**
 * Twenty Fifteen functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since Twenty Fifteen 1.0
 */

if ( ! isset( $content_width ) ) {
	$content_width = 660;
}

/**
 * Twenty Fifteen only works in WordPress 4.1 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.1-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'twentyfifteen_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since Twenty Fifteen 1.0
 */
function twentyfifteen_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on twentyfifteen, use a find and replace
	 * to change 'twentyfifteen' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'twentyfifteen', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu',      'twentyfifteen' ),
		'social'  => __( 'Social Links Menu', 'twentyfifteen' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) );

	$color_scheme  = twentyfifteen_get_color_scheme();
	$default_color = trim( $color_scheme[0], '#' );

	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'twentyfifteen_custom_background_args', array(
		'default-color'      => $default_color,
		'default-attachment' => 'fixed',
	) ) );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( 'css/editor-style.css', 'genericons/genericons.css', twentyfifteen_fonts_url() ) );
}
endif; // twentyfifteen_setup
add_action( 'after_setup_theme', 'twentyfifteen_setup' );

/**
 * Register widget area.
 *
 * @since Twenty Fifteen 1.0
 *
 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
 */
function twentyfifteen_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Widget Area', 'twentyfifteen' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentyfifteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'twentyfifteen_widgets_init' );

if ( ! function_exists( 'twentyfifteen_fonts_url' ) ) :
/**
 * Register Google fonts for Twenty Fifteen.
 *
 * @since Twenty Fifteen 1.0
 *
 * @return string Google fonts URL for the theme.
 */
function twentyfifteen_fonts_url() {
	$fonts_url = '';
	$fonts     = array();
	$subsets   = 'latin,latin-ext';

	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Noto Sans, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Noto Sans font: on or off', 'twentyfifteen' ) ) {
		$fonts[] = 'Noto Sans:400italic,700italic,400,700';
	}

	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Noto Serif, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Noto Serif font: on or off', 'twentyfifteen' ) ) {
		$fonts[] = 'Noto Serif:400italic,700italic,400,700';
	}

	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Inconsolata, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'twentyfifteen' ) ) {
		$fonts[] = 'Inconsolata:400,700';
	}

	/*
	 * Translators: To add an additional character subset specific to your language,
	 * translate this to 'greek', 'cyrillic', 'devanagari' or 'vietnamese'. Do not translate into your own language.
	 */
	$subset = _x( 'no-subset', 'Add new subset (greek, cyrillic, devanagari, vietnamese)', 'twentyfifteen' );

	if ( 'cyrillic' == $subset ) {
		$subsets .= ',cyrillic,cyrillic-ext';
	} elseif ( 'greek' == $subset ) {
		$subsets .= ',greek,greek-ext';
	} elseif ( 'devanagari' == $subset ) {
		$subsets .= ',devanagari';
	} elseif ( 'vietnamese' == $subset ) {
		$subsets .= ',vietnamese';
	}

	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => urlencode( implode( '|', $fonts ) ),
			'subset' => urlencode( $subsets ),
		), 'https://fonts.googleapis.com/css' );
	}

	return $fonts_url;
}
endif;

/**
 * JavaScript Detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Fifteen 1.1
 */
function twentyfifteen_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'twentyfifteen_javascript_detection', 0 );

/**
 * Enqueue scripts and styles.
 *
 * @since Twenty Fifteen 1.0
 */
function twentyfifteen_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'twentyfifteen-fonts', twentyfifteen_fonts_url(), array(), null );

	// Add Genericons, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.2' );

	// Load our main stylesheet.
	wp_enqueue_style( 'twentyfifteen-style', get_stylesheet_uri() );

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'twentyfifteen-ie', get_template_directory_uri() . '/css/ie.css', array( 'twentyfifteen-style' ), '20141010' );
	wp_style_add_data( 'twentyfifteen-ie', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'twentyfifteen-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'twentyfifteen-style' ), '20141010' );
	wp_style_add_data( 'twentyfifteen-ie7', 'conditional', 'lt IE 8' );

	wp_enqueue_script( 'twentyfifteen-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20141010', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'twentyfifteen-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20141010' );
	}

	wp_enqueue_script( 'twentyfifteen-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20150330', true );
	wp_localize_script( 'twentyfifteen-script', 'screenReaderText', array(
		'expand'   => '<span class="screen-reader-text">' . __( 'expand child menu', 'twentyfifteen' ) . '</span>',
		'collapse' => '<span class="screen-reader-text">' . __( 'collapse child menu', 'twentyfifteen' ) . '</span>',
	) );
}
add_action( 'wp_enqueue_scripts', 'twentyfifteen_scripts' );

/**
 * Add featured image as background image to post navigation elements.
 *
 * @since Twenty Fifteen 1.0
 *
 * @see wp_add_inline_style()
 */
function twentyfifteen_post_nav_background() {
	if ( ! is_single() ) {
		return;
	}

	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );
	$css      = '';

	if ( is_attachment() && 'attachment' == $previous->post_type ) {
		return;
	}

	if ( $previous &&  has_post_thumbnail( $previous->ID ) ) {
		$prevthumb = wp_get_attachment_image_src( get_post_thumbnail_id( $previous->ID ), 'post-thumbnail' );
		$css .= '
			.post-navigation .nav-previous { background-image: url(' . esc_url( $prevthumb[0] ) . '); }
			.post-navigation .nav-previous .post-title, .post-navigation .nav-previous a:hover .post-title, .post-navigation .nav-previous .meta-nav { color: #fff; }
			.post-navigation .nav-previous a:before { background-color: rgba(0, 0, 0, 0.4); }
		';
	}

	if ( $next && has_post_thumbnail( $next->ID ) ) {
		$nextthumb = wp_get_attachment_image_src( get_post_thumbnail_id( $next->ID ), 'post-thumbnail' );
		$css .= '
			.post-navigation .nav-next { background-image: url(' . esc_url( $nextthumb[0] ) . '); border-top: 0; }
			.post-navigation .nav-next .post-title, .post-navigation .nav-next a:hover .post-title, .post-navigation .nav-next .meta-nav { color: #fff; }
			.post-navigation .nav-next a:before { background-color: rgba(0, 0, 0, 0.4); }
		';
	}

	wp_add_inline_style( 'twentyfifteen-style', $css );
}
add_action( 'wp_enqueue_scripts', 'twentyfifteen_post_nav_background' );

/**
 * Display descriptions in main navigation.
 *
 * @since Twenty Fifteen 1.0
 *
 * @param string  $item_output The menu item output.
 * @param WP_Post $item        Menu item object.
 * @param int     $depth       Depth of the menu.
 * @param array   $args        wp_nav_menu() arguments.
 * @return string Menu item with possible description.
 */
function twentyfifteen_nav_description( $item_output, $item, $depth, $args ) {
	if ( 'primary' == $args->theme_location && $item->description ) {
		$item_output = str_replace( $args->link_after . '</a>', '<div class="menu-item-description">' . $item->description . '</div>' . $args->link_after . '</a>', $item_output );
	}

	return $item_output;
}
add_filter( 'walker_nav_menu_start_el', 'twentyfifteen_nav_description', 10, 4 );

/**
 * Add a `screen-reader-text` class to the search form's submit button.
 *
 * @since Twenty Fifteen 1.0
 *
 * @param string $html Search form HTML.
 * @return string Modified search form HTML.
 */
function twentyfifteen_search_form_modify( $html ) {
	return str_replace( 'class="search-submit"', 'class="search-submit screen-reader-text"', $html );
}
add_filter( 'get_search_form', 'twentyfifteen_search_form_modify' );

/**
 * Implement the Custom Header feature.
 *
 * @since Twenty Fifteen 1.0
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 *
 * @since Twenty Fifteen 1.0
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 *
 * @since Twenty Fifteen 1.0
 */
require get_template_directory() . '/inc/customizer.php';


/*
 * Woocommerce
 */
 
// theme support
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);

function my_theme_wrapper_start() {
  echo '<div id="primary" class="content-area"><main id="main" class="site-main" role="main">';
}

function my_theme_wrapper_end() {
  echo '</main></div>';
}

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

// check if the product is a 1 + 1 student
function is_product_student() {
	global $post;
	$project_11 = get_term_by( 'slug', '11', 'pa_project');
	$terms = get_the_terms( $post->ID, 'pa_project' );
	if ( is_array( $terms ) ) {
		foreach ( $terms as $term ) {
			if ($term->parent == $project_11->term_id) {
				return true;
			}
		}
	} else {
		return $terms->parent == $project_11->term_id;
	}
 
	return false;
}

// add short description to product listing page
add_action( 'woocommerce_after_shop_loop_item_title', 'ins_woocommerce_product_excerpt', 5, 2);
function ins_woocommerce_product_excerpt() {
 	echo '<span class="excerp">' . get_the_excerpt() . '</span>';
}

// custom oderby options
add_filter('woocommerce_catalog_orderby', 'custom_woocommerce_catalog_orderby');
 
// remove unsed and add new options to $sortby var passed into filter
function custom_woocommerce_catalog_orderby( $sortby ) {
    unset( $sortby['popularity'] );
    unset( $sortby['rating'] );
    $sortby['price'] = __( 'Sort by amount: low to high', 'ctef' );
    $sortby['price-desc'] = __( 'Sort by amount: high to low', 'ctef' );
    $sortby['stock'] = __( 'Sort by availability', 'ctef' );
    $sortby['stock-desc'] = __( 'Sort by availability reversed', 'ctef' );
    return $sortby;
}

// Change default order to "by stock"
add_filter('woocommerce_default_catalog_orderby', 'custom_default_catalog_orderby');
function custom_default_catalog_orderby() {
    return 'stock';
}

// add a "Sponsored" flash to students who has got a sponsor already
add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_sponsored_flash', 10 );
add_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_sponsored_flash', 10 );
function woocommerce_show_sponsored_flash() {
	wc_get_template( 'supported-flash.php' );
}


// remove the "Product description" header from single product page
add_filter( 'woocommerce_product_description_heading', 'filter_woocommerce_product_description_heading', 10, 1 );
function filter_woocommerce_product_description_heading( $product_description ) {
    return null;
};

// add to cart message customization
add_filter( 'wc_add_to_cart_message', 'custom_add_to_cart_message' );
function custom_add_to_cart_message() {
    global $woocommerce;

        $return_to  = get_permalink( woocommerce_get_page_id( 'cart' ) );
        $message    = sprintf( '<a href="%s" class="button wc-forwards">%s</a> %s', $return_to, __( 'View Donation', 'ctef' ), __( 'Successfully added to your donation.', 'ctef' ) );
    return $message;
}

// show sponsors on archive pages
add_action( 'woocommerce_after_shop_loop_item_title', 'wc_sponsors_show' );
function wc_sponsors_show() {
    global $product;
    
	// Do not show this on variable products
	if ( $product->product_type <> 'variable' ) {

		$sponsors = get_post_meta( $product->id, 'sponsor' );
		echo '<div class="woocommerce_sponsors">';

		if ( $sponsors ) {
			if ( is_product_student() ) {
				echo '<span class="ctef-sponsors-1">' .  __( 'Sponsored by: ', 'ctef' ) . esc_attr( $sponsors[0] ) . '</span>';
			} else {
				echo '<span class="ctef-sponsors-1">' . sprintf( __( 'Total sponsors: %d ', 'ctef' ), count( $sponsors ) ) . '</span>';
			}
		}
		
		echo '</div>';
	}
}

// show sponsor tab in product detail page, and disable "more information" tab
add_filter( 'woocommerce_product_tabs', 'my_woocommerce_default_product_tabs', 10 );
function my_woocommerce_default_product_tabs( $tabs = array() ) {
	global $product, $post;

	if ( $post->post_content ) {
		$tabs['Sponsors'] = array(
			'title'    => __( 'Sponsors', 'ctef' ),
			'priority' => 20,
			'callback' => 'my_woocommerce_product_sponosors_tab'
		);
		
		unset($tabs['additional_information']);
	}

	return $tabs;
}

function my_woocommerce_product_sponosors_tab() {
	wc_get_template( 'single-product/tabs/sponsors.php' );
}

// remove "related product" in single product page
add_filter( 'woocommerce_related_products_args', 'wc_remove_related_products', 10 ); 	
function wc_remove_related_products( $args ) {
	return array();
}

//////////////////////////////////////

// remove and modify checkout fields
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
function custom_override_checkout_fields( $fields ) {
     $fields['billing']['billing_first_name']['required'] = false;
     $fields['billing']['billing_first_name']['label'] = _x( 'Name', 'billing', 'ctef' );
     $fields['billing']['billing_first_name']['placeholder'] = __( 'Your name', 'ctef' );
     $fields['billing']['billing_email']['required'] = true;
     $fields['billing']['billing_email']['placeholder'] = __( 'Your email', 'ctef' );
     unset( $fields['billing']['billing_last_name'] );
     unset( $fields['billing']['billing_company'] );
     unset( $fields['billing']['billing_address_1'] );
     unset( $fields['billing']['billing_address_2'] );
     unset( $fields['billing']['billing_city'] );
     unset( $fields['billing']['billing_postcode'] );
     unset( $fields['billing']['billing_country'] );
     unset( $fields['billing']['billing_state'] );
     unset( $fields['billing']['billing_phone'] );
     
     $fields['order']['order_comments']['placeholder'] = __( 'Notes about your donation.', 'ctef' );
     
     return $fields;
}

// add attribute of sponsor
add_action( 'woocommerce_thankyou', 'set_the_sponsor' );
function set_the_sponsor( $order_id ) {

	// grab the order
	$order = WC_Order_Factory::get_order( $order_id );

	$items = $order->get_items();
	$sponsor = esc_html( $order->billing_first_name );
	if ( !$sponsor ) {
		$order->set_address( array( 'first_name' => __( 'Anonymous sponsor', 'ctef' ) ), 'billing' );
	}
	
	foreach ( $items as $item ) {
	    $product_id = $item['product_id'];
	    add_post_meta( $product_id, 'sponsor', $order->billing_first_name  );
	}
}

// do not display subtotal as this information is redundant
add_filter( 'woocommerce_order_subtotal_to_display', 'hide_subtotal' );
function hide_subtotal () {
	return false;
}

// renaming

// view cart -> view donation
add_filter( 'gettext', 'view_cart_to_view_donation', 10, 3 );
function view_cart_to_view_donation( $translation, $text, $domain ) {

	if ( $domain == 'woocommerce' ) {
	   if ( $text == 'View Cart' ) {
	       $translation = __('View Donation', 'ctef' );
	   }
	}
	
	return $translation;
}

// products -> donate
add_filter( 'wp_title', 'wc_custom_shop_archive_title' );
function wc_custom_shop_archive_title( $title ) {
    return str_replace( __( 'Products', 'woocommerce' ),  __( 'Donate', 'ctef' ), $title );
}

// shop -> donate
add_filter( 'woocommerce_page_title', 'rename_shop_to_donate' );
function rename_shop_to_donate( $page_title ) {
	return __( 'Donate', 'ctef' );
}

// add to cart -> add to donate (on single product page)
add_filter( 'woocommerce_product_single_add_to_cart_text', 'add_to_cart_to_donate' );
function add_to_cart_to_donate($obj) {
	return __( 'Donate', 'ctef' );
}

// add to cart -> Donate if available (on archieve page)
add_filter( 'woocommerce_product_add_to_cart_text', 'product_add_to_cart_to_donate' );
function product_add_to_cart_to_donate( $text ) {
	global $product;

	if ( $product->is_in_stock() ) {
		return __( 'Donate', 'ctef' );
	}
	
	return $text;
}

add_filter ( 'woocommerce_add_error', 'cannot_add_product' );
function cannot_add_product( $text ) {
	return __( "Can't add to donation, it's already in your list or not available at the time.", 'ctef' );
}

// localize titles
function localized_title(){
	if ( is_cart() ) {
		echo '<h1 class="entry-title">' . __( 'Donation List', 'ctef' ) . '</h1>';
	} else if ( is_checkout() ) {
		echo '<h1 class="entry-title">' . __( 'Checkout', 'woocommerce' ) . '</h1>';
	} else if ( is_shop() ) {
		echo '<h1 class="entry-title">' . __( 'Donate', 'ctef' ) . '</h1>';
	} else {
		echo the_title( '<h1 class="entry-title">', '</h1>' );
	}
}

// in stock -> available
// out of stock -> sponsored
add_filter( 'woocommerce_get_availability', 'custom_get_availability', 1, 2);
function custom_get_availability( $availability,  $product) {

	if ($availability['class'] == 'out-of-stock') {
		$availability['availability'] = __( 'Sponsored', 'ctef' );
	} else {
		$availability['availability'] = __( 'Available', 'ctef' );
	}
	
  	return $availability;
}

// Order Received -> donation received
add_filter( 'the_title', 'title_order_received_to_donation_received', 10, 2 );
function title_order_received_to_donation_received( $title, $id ) {
	if ( is_order_received_page() && get_the_ID() === $id ) {
		$title = __( 'Donation Received', 'ctef' );
	}
	return $title;
}

//////////////////////////////////////

// load the language pack for ctef
add_action( 'init', 'load_custom_textdomain' );
function load_custom_textdomain() {
	$domain = 'ctef';
	$locale = apply_filters( 'plugin_locale', get_locale(), $domain );
	if ( $loaded = load_textdomain( $domain, trailingslashit( WP_LANG_DIR ) . $domain . '/' . $domain . '-' . $locale . '.mo' ) ) {
		return $loaded;
	} else {
		load_plugin_textdomain( $domain, FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
	}
}

// Ensure cart contents update when products are added to the cart via AJAX
add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );
function woocommerce_header_add_to_cart_fragment( $fragments ) {
	
	global $woocommerce;
	ob_start();
	
	$cart_url = $woocommerce->cart->get_cart_url();
    if ( $woocommerce->cart->is_empty() ) {
		echo '<a class="cart-contents float-right" href="' . $cart_url . '">' . __('My Donation', 'ctef') . ' <i class="fa fa-heart-o my-donation"></i> ($0)</a>';
	} else {
		echo '<a class="cart-contents float-right" href="' . $cart_url . '">' . __('My Donation', 'ctef') . ' <i class="fa fa-heart my-donation"></i> (' . $woocommerce->cart->get_cart_total() . ')</a>';
	}

	$fragments['a.cart-contents'] = ob_get_clean();
	
	return $fragments;
}

// customize paypal button, to remove "alt" class
add_filter( 'woocommerce_order_button_html', 'customize_checkout_button');
function customize_checkout_button( $default ) {
	return str_replace('class="button alt"', 'class="button"', $default);
} 

// remove breadcrumb for homepage
add_filter( 'woocommerce_breadcrumb_defaults', 'remove_breadcrumb_home', 1 );
function remove_breadcrumb_home( $defaults ) {
	if (!is_shop()) {
		return $defaults;
	} else {
		$defaults['wrap_before'] = '<nav class="woocommerce-breadcrumb float-left" itemprop="breadcrumb">';
		return $defaults;
	}
}