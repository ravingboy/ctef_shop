<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentyfifteen' ); ?></a>
	<div id="topbar" class="bottom topbar site-branding">
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_template_directory_uri() . '/images/label.png'; ?>" class="site-logo float-left"></a>
		<span class="site-title float-left">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a> <br />
			<span class="site-description"><?php bloginfo( 'description', 'display' ); ?></span>
		</span>
		
		<?php
		if ( !is_cart() && !is_checkout() ) {
			global $woocommerce;
			
			// get cart url
			$cart_url = $woocommerce->cart->get_cart_url();
			
			if ( $woocommerce->cart->is_empty() ) {
			    echo '<a class="cart-contents float-right" href="' . $cart_url . '">' . __('My Donation', 'ctef') . ' <i class="fa fa-heart-o my-donation"></i> ($0)</a>';
			} else {
				echo '<a class="cart-contents float-right" href="' . $cart_url . '">' . __('My Donation', 'ctef') . ' <i class="fa fa-heart my-donation"></i> (' . $woocommerce->cart->get_cart_total() . ')</a>';
			}
		} ?>
		<br />
		<button class="secondary-toggle"><?php _e( 'Menu and widgets', 'twentyfifteen' ); ?></button>
		<div class="float-right secondary">
			<div class="yith-ajaxsearchform-container">
			    <form role="search" method="get" id="yith-ajaxsearchform" action="<?php site_url(); ?>">
					<div class="input-group form-search">
			            <label class="screen-reader-text input-group-element" for="yith-s">Search for:</label>
				  		<input type="search" value="" name="s" id="yith-s" class="yith-s input-group-element" placeholder="搜索学校或学生" data-loader-icon="" data-min-chars="3" autocomplete="off">
    					<div class="input-group-element">
				            <input type="submit" id="yith-searchsubmit" value="搜索">
				  		</div>
			            <input type="hidden" name="post_type" value="product">
			        </div>
			    </form>
			<div class="autocomplete-suggestions" style="position: absolute; display: none; max-height: 300px; z-index: 9999;"></div></div>
		</div>
	</div>

	<div id="sidebar" class="sidebar">
		<?php get_sidebar(); ?>
	</div><!-- .sidebar -->

<?php if ( is_shop() ) : ?>
	<div id="content" class="site-content">
<?php else : ?>
    <div id="content" class="site-content site-content-single-column">
<?php endif; ?>
