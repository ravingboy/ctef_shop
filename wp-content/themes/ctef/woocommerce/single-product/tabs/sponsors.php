<?php
/**
 * Description tab
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;

$sponsors = get_post_meta( $post->ID, 'sponsor' );

if ( !$sponsors ) {
    _e( 'Not sponsored yet.', 'ctef' );
}

foreach ( $sponsors as $sponsor ) {
    echo esc_attr( $sponsor ) . '<br />';
}
