��    *      l  ;   �      �     �     �  	   �  N   �          "     +     <     J     [     m     }     �  ;   �     �     �  #   �          .     M     i     �     �     �  	   �     �  
   �     �  $   �  +        F  m   Z     �     �     �  !   �  
     	     g   "     �     �  �  �     /	     6	     I	  f   Y	     �	     �	     �	     �	  	   �	     �	     
     
  	   
  '   &
     N
  '   a
     �
     �
     �
     �
     �
  '     '   C     k     {     �     �  	   �  -   �  *   �       U        p     w     �  '   �     �     �  i   �     I     P                  "                     *                              (                	                 #       %                      '   $                 &   !         )                            
       Amount Anonymous sponsor Available Can't add to donation, it's already in your list or not available at the time. Donate Donation Donation Details Donation List Donation Number: Donation Received Donation Totals My Donation Name: No projects or students were found matching your selection. Not sponsored yet. Notes about your donation. Please attempt your donation again. Return To Donate Search Projects or Students... Sort by amount: high to low Sort by amount: low to high Sort by availability Sort by availability reversed Sponsor Details Sponsored Sponsored by:  Sponsored! Sponsors Successfully added to your donation. Thank you. Your donation has been received. Total sponsors: %d  Unfortunately your donate cannot be processed as the originating bank/merchant has declined your transaction. Update View Donation Your Donation Your donation is currently empty. Your email Your name Your name will be dispalyed on our website as sponsor of the projects below, or you can leave it empty. billingName productName Project-Id-Version: 
POT-Creation-Date: 2015-12-07 00:12-0800
PO-Revision-Date: 2016-01-04 00:03-0800
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.6
X-Poedit-Basepath: .
Plural-Forms: nplurals=1; plural=0;
Language: zh_CN
X-Poedit-KeywordsList: ctef;Donate;Donation
X-Poedit-SearchPath-0: .
 金额 匿名爱心人士 可接受捐助 无法添加捐助项目，可能它已经在您的捐助列表中，或者这个项目已经被捐助 捐助 捐助 捐助详情 捐助列表 序号： 捐助已收到 捐助总计 我的捐助 姓名： 没有搜索到相关项目或者学生 尚未收到捐助 您的捐助留言，仅管理员可见 请重新提交您的捐助。 返回捐助项目列表 搜索项目或者学生... 按金额排序：从高到低 按金额排序：从低到高 按状态排序：可捐助项目在前 按状态排序：可捐助项目在后 捐助人详情 已受捐助 资助人： 已受捐助 资助人 项目已成功添加到您的捐助列表中 非常感谢！您的捐助已经收到。 资助人总数: %d  抱歉，您的捐助未能成功，因为发起银行/商户拒绝了您的请求。 刷新 查看捐助 您的捐助详情 您目前还没有需要捐助的项目 您的电子邮箱 您的姓名 您的名字将被显示在项目的“资助人”列表中；如果您想匿名捐赠，此处请留空 姓名 名称 